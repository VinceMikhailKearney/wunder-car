# Wunder Car Project

An iOS app using RxSwift, RxCocoa and Realm that pulls down a list of cars using the link below:

https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json

## Layout and Navigation

Auto Layout on one Storyboard was used to manage the apps layout.

The MapView is presented modally as a Navigation stack was not necessary.

## Pods

All pods are listed below:

### App

pod 'RxSwift', '~> 5'

pod 'RxCocoa', '~> 5'

pod "RxGesture"

pod 'RealmSwift'

pod 'JGProgressHUD'

pod 'BulletinBoard'

### Testing

pod 'RealmSwift'

pod 'RxBlocking', '~> 5'

pod 'RxTest', '~> 5'
