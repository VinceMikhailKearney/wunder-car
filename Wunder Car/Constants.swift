//
//  Constants.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 22/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import MapKit

struct FakeData {
    private static let fakeCarOneDict = ["name": "Skoda",
                                     "address": "My Addreess",
                                     "exterior": "GOOD",
                                     "interior": "GOOD",
                                     "coordinates": [1.0, 2.0, 0],
                                     "vin": "55720kfnvn21128e4bh",
                                     "fuel": 42] as [String : Any]
    
    private static let fakeCarTwoDict = ["name": "VW",
                                         "address": "My Addreess",
                                         "exterior": "GOOD",
                                         "interior": "GOOD",
                                         "coordinates": [1.0, 2.0, 0],
                                         "vin": "193uyrbvbmwvl1oy3",
                                         "fuel": 65] as [String : Any]
    
    static let fakeCarOne = Car.createFake(withDictionary: fakeCarOneDict)
    static let fakeCarTwo = Car.createFake(withDictionary: fakeCarTwoDict)
    
    static var carArray: [Car] {
        return carList.map { Car.createFake(withDictionary: $0) } as! [Car]
    }
    
    static let carList =
        [
            ["name": "Volvo",
                "address": "My Addreess",
                "exterior": "GOOD",
                "interior": "GOOD",
                "coordinates": [1.0, 2.0, 0],
                "vin": "5425WECNKA123",
                "fuel": 32],
            ["name": "Ford",
                "address": "My Addreess",
                "exterior": "GOOD",
                "interior": "GOOD",
                "coordinates": [1.0, 2.0, 0],
                "vin": "012832htgbbsksdi12i",
                "fuel": 12],
            fakeCarOneDict,
            fakeCarTwoDict
        ]
}

struct MapData {
    static let regionMeters: CLLocationDistance = 1_000
}
