//
//  CustomAnnoView.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 22/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import RxCocoa

private let kAnimateTime = 0.5

class CustomAnnoView: MKAnnotationView {
    weak var car: Car?
    weak var customCalloutView: UIView?
    override var annotation: MKAnnotation? {
        willSet { customCalloutView?.removeFromSuperview() }
    }
    var isSelectedBinder = BehaviorRelay<Bool>(value: false)
    
    // MARK: - Lifecycle
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        // This way, we don't show default callout
        self.canShowCallout = false
        self.image = UIImage(named: "pin")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // This way, we don't show default callout
        self.canShowCallout = false
        self.image = UIImage(named: "pin")
    }
    
    // MARK: - Custom Callout Showing/Hiding
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.customCalloutView?.removeFromSuperview()
            if let newView = loadCustomCallout() {
                newView.frame.origin.x -= newView.frame.width / 2.0 - (self.frame.width / 2.0)
                newView.frame.origin.y -= newView.frame.height + 10
                
                addSubview(newView)
                customCalloutView = newView
                
                if animated {
                    customCalloutView?.alpha = 0.0
                    UIView.animate(withDuration: kAnimateTime) {
                        self.customCalloutView?.alpha = 1.0
                    }
                }
            }
        } else {
            if animated {
                UIView.animate(withDuration: kAnimateTime, animations: {
                    self.customCalloutView?.alpha = 0.0
                }, completion: { (success) in
                    self.customCalloutView?.removeFromSuperview()
                })
            } else {
                customCalloutView?.removeFromSuperview()
            }
        }
        
        isSelectedBinder.accept(selected)
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // We need to know whether the tap is on the CalloutView or the AnnotationView itself
        if let result = customCalloutView?.hitTest(convert(point, to: customCalloutView), with: event) {
            isSelected = false
            return result
        }
        return self
    }
    
    private func loadCustomCallout() -> CustomCalloutView? {
        guard let views = Bundle.main.loadNibNamed("CustomCalloutView", owner: self, options: nil) as? [CustomCalloutView],
            let view = views.first else { return nil }
        
        view.carName.text = car?.name ?? "None"
        return view
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        customCalloutView?.removeFromSuperview()
    }
}
