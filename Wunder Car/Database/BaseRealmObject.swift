//
//  BaseRealmObject.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 21/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import Foundation
import RealmSwift

enum baseRealmError: Error
{ case realmTypeMustBeOverridden }

class BaseRealmObject: Object {
    // MARK: Properties
    fileprivate static var baseRealm: Realm?
    
    // MARK: Return the type of realm object --- MUST BE OVERRIDDEN IN SUBCLASSES.
    class func realmType() throws -> Object.Type {
        throw baseRealmError.realmTypeMustBeOverridden
    }
    
    // MARK: Use a base Realm
    class func base() -> Realm {
        if baseRealm == nil {
            baseRealm = try! Realm()
        }
        return baseRealm!
    }
    
    // MARK: Saving and deleting realm objects
    func save() {
        let realm = BaseRealmObject.base()
        try! realm.write({
            realm.add(self)
        })
    }
    
    static func deleteObject(_ object: Object) {
        let realm = base()
        try! realm.write({
            realm.delete(object)
        })
    }
    
    static func deleteAllObjects() {
        let realm = base()
        let objects = realm.objects(try! realmType())
        try! realm.write({
            realm.delete(objects)
        })
    }
    
    // MARK: Helper methods
    static func allObjects() -> Array<Object> {
        return Array(base().objects(try! realmType()))
    }
}
