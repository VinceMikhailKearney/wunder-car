//
//  Car.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 21/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import Foundation
import MapKit
import RealmSwift

class Car: BaseRealmObject
{
    @objc dynamic var key: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var address: String = ""
    @objc dynamic var exterior: String = ""
    @objc dynamic var interior: String?
    @objc dynamic var fuel: Int = 0
    @objc dynamic var long: Double = 0.0
    @objc dynamic var lat: Double = 0.0

    // MARK: Overrides
    override class func realmType() throws -> Object.Type {
        return Car.self
    }

    open override class func primaryKey() -> String? {
        return "key"
    }

    // MARK: Creating
    static func create(withDictionary dict: [String: Any]) -> Car? {
        guard let vin = dict["vin"] as? String else { return nil }

        let realm = BaseRealmObject.base()
        var car: Car? = realm.object(ofType: Car.self, forPrimaryKey: vin)

        if car == nil {
            let newCar = Car()
            newCar.key = vin
            car = newCar
        }
        car?.assign(dictionary: dict)
        car?.save()

        return car
    }

    func assign(dictionary dict: [String: Any]) {
        let realm = BaseRealmObject.base()
        realm.beginWrite()
        address = dict["address"] as? String ?? ""
        exterior = dict["exterior"] as? String ?? ""
        interior = dict["interior"] as? String ?? ""
        name = dict["name"] as? String ?? ""
        fuel = dict["fuel"] as? Int ?? 0
        if let coords = dict["coordinates"] as? [Double] {
            long = coords[0]
            lat = coords[1]
        }
        do {
            try realm.commitWrite()
        } catch {
            print("Error committing write: \(error)")
        }
    }
    
    static func createFake(withDictionary dict: [String: Any]) -> Car? {
        guard let vin = dict["vin"] as? String else { return nil }
        let car = Car()
        car.key = vin
        car.assign(dictionary: dict)
        
        return car
    }

    var location: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: CLLocationDegrees(lat),
                                      longitude: CLLocationDegrees(long))
    }
}
