//
//  MkCoordinateRegion+Utils.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 23/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import MapKit

// Extension added to allow comparison of two MKCoordinateRegion objects in Unit Tests.
extension MKCoordinateRegion: Equatable {
    public static func == (lhs: MKCoordinateRegion, rhs: MKCoordinateRegion) -> Bool {
        if lhs.center.latitude != rhs.center.latitude
            || lhs.center.longitude != rhs.center.longitude {
            return false
        }
        
        if lhs.span.latitudeDelta != rhs.span.latitudeDelta
            || lhs.span.longitudeDelta != rhs.span.longitudeDelta {
            return false
        }
        
        return true
    }
}
