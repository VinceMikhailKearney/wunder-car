//
//  Utils.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 21/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import UIKit

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

// By default, make every UIViewController conform to this
extension UIViewController: StoryboardIdentifiable {}

extension UIStoryboard {
    class func instantiateViewController<VC: UIViewController>(_ identifier: String = "", fromBoard board: String = "Main") -> VC {
        let board = UIStoryboard(name: board, bundle: nil)
        
        if identifier.isEmpty {
            guard let viewController = board.instantiateInitialViewController() as? VC else {
                fatalError("Couldn't instantiate the intial view controller of board \(board)")
            }
            return viewController
        }

        guard let viewController = board.instantiateViewController(withIdentifier: identifier) as? VC else {
            fatalError("Couldn't instantiate view controller with identifier \(identifier)")
        }
        return viewController
    }
    
    class func mapViewController() -> MapViewController {
        return UIStoryboard.instantiateViewController(MapViewController.storyboardIdentifier)
    }
}
