//
//  CarAnnotation.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 22/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import MapKit

class CarAnnotation: NSObject, MKAnnotation {
    var car: Car
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude:  CLLocationDegrees(car.lat),
                                      longitude: CLLocationDegrees(car.long))
    }
    
    init(car: Car) {
        self.car = car
        super.init()
    }
    
    var title: String? {
        return car.name
    }
    
    var subtitle: String? {
        return car.key
    }
}

