//
//  MainViewModel.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 22/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class CarListViewModel: NSObject {
    var carListObservable: BehaviorRelay<[Car]> = BehaviorRelay(value: Car.allObjects() as? [Car] ?? [])
    var showHudForFetchingCars: BehaviorRelay<Bool> = BehaviorRelay(value: true)
    var apiService: NetworkProtocol!
    let disposeBag = DisposeBag()
    
    init(withApiService service: NetworkProtocol) {
        apiService = service
    }
    
    func getCars() {
        apiService.getCars()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [unowned self] cars in
                self.showHudForFetchingCars.accept(false)
                self.persistCars(cars)
            }, onError: { error in
                self.showHudForFetchingCars.accept(false)
                print("Woops! Something went wrong getting the cars -> \(error)")
            }).disposed(by: disposeBag)
    }
    
    func persistCars(_ cars: [[String: Any]]) {
        var listOfCars: [Car] = []
        cars.forEach {
            if let newCar = Car.create(withDictionary: $0) {
                listOfCars.append(newCar)
            }
        }
        self.carListObservable.accept(listOfCars)
    }
}
