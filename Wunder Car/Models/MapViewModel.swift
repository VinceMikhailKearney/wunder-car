//
//  MapViewModel.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 22/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import Foundation
import MapKit
import RxSwift
import RxCocoa

class MapViewModel: NSObject {
    var selectedCar: Car!
    let disposeBag = DisposeBag()
    let annotationReuseId = "CustomAnnoView"
    
    init(withCar car: Car) {
        selectedCar = car
    }
    
    var selectedKey: String {
        return selectedCar.key
    }
    
    func selectedCarRegion() -> MKCoordinateRegion {
        return MKCoordinateRegion(center: selectedCar.location,
                                  latitudinalMeters: MapData.regionMeters,
                                  longitudinalMeters: MapData.regionMeters)
    }
    
    func carAnnotations(withObjects objects: [Any] = Car.allObjects()) -> [CarAnnotation] {
        guard let allCars = objects as? [Car] else {
            return []
        }
        return allCars.map { CarAnnotation(car: $0) }
    }
    
    func carNotMatchSelected(_ car: Car?) -> Bool {
        return car?.key != selectedKey
    }
    
    func carMatchSelected(_ car: Car?) -> Bool {
        return car?.key == selectedKey
    }
}
