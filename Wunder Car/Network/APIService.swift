//
//  APIService.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 21/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import Foundation
import RxSwift

class APIService: NSObject, NetworkProtocol {
    static let shared = APIService()
    
    func getCars() -> Observable<[[String: Any]]> {
        return Observable.create { observer -> Disposable in
            let url = URL(string: "https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json")!
            URLSession.shared.dataTask(with: URLRequest(url: url))
            { (data, response, error) in
                if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: [[String: Any]]],
                    let carList = json["placemarks"]
                {
                    if let error = error { observer.onError(error); return }
                    
                    guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                        // Something else has gone wrong, return empty list
                        observer.onNext([[:]])
                        return
                    }
                    
                    observer.onNext(carList)
                } else {
                    // We were unable to get anything back, so send back an empty list
                    observer.onNext([[:]])
                }
            }.resume()
            
            return Disposables.create()
        }
    }
    
    // Non-Reactive initial approach below
    func getCarList(_ completion: @escaping (_ result : [[String: Any]]?) -> Void) {
        APIService.shared.fetchJson(fromUrl: URL(string: "https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json")!) { response in
            completion(response)
        }
    }
    
    func fetchJson(fromUrl url : URL, completion: @escaping (_ result : [[String: Any]]?) -> Void) {
        URLSession.shared.dataTask(with: URLRequest(url: url))
        { (data, response, error) in
            if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: [[String: Any]]], let carList = json["placemarks"] {
                completion(carList)
            }
            completion(nil)
        }.resume()
    }
}
