//
//  MockApiService.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 22/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import Foundation
import RxSwift

enum CustomError: Error {
    case testCaseGenericFailed
}

enum MockResultType {
    case success, failure
}

class MockApiService: NSObject, NetworkProtocol {
    var resultType: MockResultType!
    init(withResultType type: MockResultType) {
        resultType = type
    }
    
    func getCars() -> Observable<[[String : Any]]> {
        return Observable.create { observer -> Disposable in
            switch self.resultType {
                case .success:
                    observer.onNext(FakeData.carList)
                case .failure, .none:
                    observer.onError(CustomError.testCaseGenericFailed)
                
            }
            return Disposables.create()
        }
    }
}
