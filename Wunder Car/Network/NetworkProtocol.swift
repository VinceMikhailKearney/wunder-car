//
//  NetworkProtocol.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 22/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import RxSwift

protocol NetworkProtocol {
    func getCars() -> Observable<[[String: Any]]>
}
