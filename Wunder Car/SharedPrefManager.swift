//
//  SharedPrefManager.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 23/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import Foundation

class SharedPrefManager: NSObject {
    static let showIntroPageKey = "ShownIntroPageKey"
    static let showMapIntroPageKey = "ShownMapIntroPageKey"
    
    private static let standard = UserDefaults.standard
    
    static func applyDefaults() {
        standard.register(defaults: [showIntroPageKey: true])
        standard.register(defaults: [showMapIntroPageKey: true])
    }
    
    static func showIntroPage() -> Bool {
        return standard.bool(forKey: showIntroPageKey)
    }
    
    static func setShownIntroPage() {
        standard.set(false, forKey: showIntroPageKey)
    }
    
    static func showMapIntroPage() -> Bool {
        return standard.bool(forKey: showMapIntroPageKey)
    }
    
    static func setShownMapIntroPage() {
        standard.set(false, forKey: showMapIntroPageKey)
    }
}
