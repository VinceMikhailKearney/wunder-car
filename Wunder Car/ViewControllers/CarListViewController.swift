//
//  ViewController.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 21/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import RxCocoa
import JGProgressHUD
import BLTNBoard

// TODO: Would be nice to use SwiftUI for designing this view instead.
class CarListViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    private let viewModel = CarListViewModel(withApiService: APIService.shared)
    private var hud: JGProgressHUD = {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Fetching Cars..."
        return hud
    }()
    
    // I have added a wee UI element as an intro page. I just thought it'd be nice to have something neat to introduce the app!
    // Don't worry - It'll show the first time that it's launched
    private lazy var bulletinManager: BLTNItemManager = {
        let page = BLTNPageItem(title: "Welcome to Wunder Car")
        page.image = UIImage(named: "car")
        page.descriptionText = "Welcome to the project! We can see a list of cars and a map with each of them added. Tapping the pins can allow us to see the name of the car :)"
        page.actionButtonTitle = "Let's Go!"
        page.alternativeButtonTitle = "Don't worry, only shown once!"
        page.isDismissable = false
        page.requiresCloseButton = false
        
        page.actionHandler = { item in
            // When we tap 'Let's Go', go fetch the cars now :)
            self.fetchCars()
            SharedPrefManager.setShownIntroPage() // We've shown it now, so don't show every time.
            item.manager?.dismissBulletin(animated: true)
        }
        
        let manager = BLTNItemManager(rootItem: page)
        manager.backgroundViewStyle = .blurredDark
        return manager
    }()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Rx Setup
        configTableView()
        setUpCellTapping()
        
        if !SharedPrefManager.showIntroPage() {
            // If we have already shown the page, then just go ahead and fetch the list
            fetchCars()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if SharedPrefManager.showIntroPage() {
            bulletinManager.showBulletin(above: self)
        }
    }
    
    private func fetchCars() {
        // Add observable to indicate whether we need to show the HUD or not
        viewModel.showHudForFetchingCars.asObservable().subscribe(onNext: { show in
            show ? self.hud.show(in: self.view) : self.hud.dismiss()
        }).disposed(by: viewModel.disposeBag)
        // Now go get the list of cars :)
        viewModel.getCars()
    }
}

// MARK: - RxSwift Setup
extension CarListViewController {
    func configTableView() {
        viewModel.carListObservable.bind(to: tableView.rx.items(cellIdentifier: "TableCell", cellType: UITableViewCell.self))
        { row, car, cell in
            cell.textLabel?.text = car.address
        }.disposed(by: viewModel.disposeBag)
    }

    func setUpCellTapping() {
        tableView.rx.modelSelected(Car.self).subscribe(onNext: { [unowned self] car in
            let mapViewController = UIStoryboard.mapViewController()
            mapViewController.viewModel = MapViewModel(withCar: car)
            self.present(mapViewController, animated: true, completion: nil)

            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }).disposed(by: viewModel.disposeBag)
    }
}
