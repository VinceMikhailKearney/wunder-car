//
//  MapViewController.swift
//  Wunder Car
//
//  Created by Vincent Kearney on 21/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import MapKit
import UIKit
import RxSwift
import RxCocoa
import RxGesture
import BLTNBoard

// TODO: Would be nice to use SwiftUI for designing this view instead.
class MapViewController: UIViewController {
    // MARK: - Outlets & Properties
    @IBOutlet weak var mapView: MKMapView! {
        didSet {
            mapView.delegate = self
        }
    }
    @IBOutlet weak var closeButton: UIButton!
    var viewModel: MapViewModel!
    private var selectedCarAnnotation: CarAnnotation?
    
    private lazy var bulletinManager: BLTNItemManager = {
        let page = BLTNPageItem(title: "Welcome to the Map View")
        page.image = UIImage(named: "map")
        page.descriptionText = "Here we can see all the cars pinned on the map! Go ahead and tap one of the car names :)"
        page.actionButtonTitle = "Ok, I'll give it a go 🤔"
        page.alternativeButtonTitle = "Don't worry, only shown once too!"
        page.isDismissable = false
        page.requiresCloseButton = false
        
        page.actionHandler = { item in
            item.manager?.dismissBulletin(animated: true)
        }
        
        let manager = BLTNItemManager(rootItem: page)
        manager.backgroundViewStyle = .blurredDark
        return manager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCloseButton()
        
        mapView.addAnnotations(viewModel.carAnnotations())
        mapView.setRegion(mapView.regionThatFits(viewModel.selectedCarRegion()), animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if SharedPrefManager.showMapIntroPage() {
            bulletinManager.showBulletin(above: self, animated: true) {
                SharedPrefManager.setShownMapIntroPage()
            }
        }
    }
    
    private func showBulletin(forView view: CustomAnnoView) {
        guard let car = view.car else { return }
        
        let page = BLTNPageItem(title: car.name)
        page.descriptionText = "Name: \(car.name)\nAddress: \(car.address)\nFuel: \(car.fuel)\nExterior: \(car.exterior)\nInterior: \(car.exterior)"
        page.isDismissable = false
        page.requiresCloseButton = false
        page.actionButtonTitle = "Dismiss"
        page.actionHandler = { item in
            self.mapView.selectAnnotation(view.annotation!, animated: false)
            page.manager?.dismissBulletin(animated: true)
        }
        
        bulletinManager = BLTNItemManager(rootItem: page)
        bulletinManager.showBulletin(above: self)
    }
}

// MARK: - Rx Setup
extension MapViewController {
    func setUpCloseButton() {
        closeButton.rx.tap.bind {
            self.dismiss(animated: true, completion: nil)
        }.disposed(by: viewModel.disposeBag)
    }
}

// MARK: - MKMapViewDelegate
extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: MKUserLocation.self) { return nil }
        guard let carAnnotation = annotation as? CarAnnotation else { return nil }
        
        var dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: viewModel.annotationReuseId) as? CustomAnnoView
        if dequeuedView == nil {
            dequeuedView = CustomAnnoView(annotation: annotation, reuseIdentifier: viewModel.annotationReuseId)
        } else {
            dequeuedView?.annotation = annotation
        }
        dequeuedView?.car = carAnnotation.car
        if viewModel.carMatchSelected(carAnnotation.car) {
            selectedCarAnnotation = carAnnotation
        }
        
        return dequeuedView
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        for case let customView as CustomAnnoView in views {
            guard let selectedAnno = self.selectedCarAnnotation else { return }
            let mainSelectedView = mapView.view(for: selectedAnno)
            
            customView.isSelectedBinder.subscribe(onNext: { bool in
                // We only need to be concerned when we are showing a new callout
                guard bool == true else { return }

                // Add a Tap Gesture to each of the CallOut Views. I want to show a BulletinBoard when tapped to show all the relevant info :)
                customView.customCalloutView?.rx.tapGesture().when(.recognized).subscribe(onNext: { _ in
                    self.showBulletin(forView: customView)
                    self.mapView.selectAnnotation(customView.annotation!, animated: false)
                }).disposed(by: self.viewModel.disposeBag)

                if customView != mainSelectedView {
                    mapView.deselectAnnotation(selectedAnno, animated: true)
                }
            }).disposed(by: viewModel.disposeBag)
            
            if customView == mainSelectedView {
                mapView.selectAnnotation(selectedAnno, animated: true)
            }
        }
    }
}
