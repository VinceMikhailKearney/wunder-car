//
//  Wunder_CarTests.swift
//  Wunder CarTests
//
//  Created by Vincent Kearney on 21/09/2019.
//  Copyright © 2019 Vincent Kearney. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RxTest
import RxBlocking
import RealmSwift
import MapKit

@testable import Wunder_Car

class Wunder_CarTests: XCTestCase {
    var scheduler: TestScheduler!
    var carListViewModel: CarListViewModel!
    var mapViewModel: MapViewModel!
    var disposeBag: DisposeBag!

    override func setUp() {
        try! wipeDatabase()
        scheduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
            
        // The default case will have the result type set as successful
        let mockService = MockApiService(withResultType: .success)
        carListViewModel = CarListViewModel(withApiService: mockService)
        
        guard let fakeCar = FakeData.fakeCarOne else {
            fatalError("Unable to get a fake car... It's needed")
        }
        mapViewModel = MapViewModel(withCar: fakeCar)
    }

    // MARK: - CarListViewModel Tests
    func testThatAppStartsWithNoCarsInDatabase() throws {
        // When the viewModel is created it should have an empty list. There are no Car Objects
        // persisted, so there shouldn't be anything.
        XCTAssertEqual(try carListViewModel.carListObservable.toBlocking().first(), [])
    }

    func testPersistJsonList() throws {
        let arrayCount = scheduler.createObserver(Array<Car>.self)
        
        carListViewModel.carListObservable
            .asObservable()
            .subscribe(arrayCount)
            .disposed(by: disposeBag)
        
        // The fake list has 4 cars, so the DB and carListObservable should be updated
        carListViewModel.persistCars(FakeData.carList)
        let carList = Car.allObjects() as! [Car]
        
        scheduler.start()
        
        // Now we need to check the array of the last event observed.
        // It should match the list of cars that are in the DB.
        XCTAssertEqual(arrayCount.events.last?.value.element, carList)
    }
    
    func testSuccessfulApiCarFetch() {
        // As the default type is .success, we can just perform the call without altering anything
        let successExpectation = expectation(description: "We should have got the list")
        successExpectation.expectedFulfillmentCount = 2
        
        carListViewModel.carListObservable.asObservable().debug().subscribe(onNext: { value in
            // We will see both an empty array and then a filled array once the
            // getCars() call has successfully completed in this case.
            successExpectation.fulfill()
        }).disposed(by: disposeBag)
        
        carListViewModel.getCars()
        
        wait(for: [successExpectation], timeout: 0.1)
        
        // The success call of the MockApi will produce the same results from FakeData.carList.
        // We can assert that the viewModel contains the same data that has therefore been persisted.
        XCTAssertEqual(carListViewModel.carListObservable.value, Car.allObjects() as! [Car])
    }
    
    func testFailedApiCarFetch() {
        let failedService = MockApiService(withResultType: .failure)
        carListViewModel = CarListViewModel(withApiService: failedService)
        
        // As the getCars() call will fail here, the observer will only see ONE event
        let failedExpectation = expectation(description: "We should see only see an empty array")
        failedExpectation.expectedFulfillmentCount = 1
        
        carListViewModel.carListObservable.asObservable().debug().subscribe(onNext: { value in
            failedExpectation.fulfill()
        }).disposed(by: disposeBag)
        
        carListViewModel.getCars()
        
        wait(for: [failedExpectation], timeout: 0.1)
    }
    
    // MARK: - MapViewModel Tests
    func testFakeCarSelected() {
        // The MapViewModel is set up with FakeData.fakeCarOne. Therefore, this should be true if
        // we check if this is the selected car.
        XCTAssertTrue(mapViewModel.carMatchSelected(FakeData.fakeCarOne))
    }
    
    func testFakeCarNotSelected() {
        // The MapViewModel is set up with FakeData.fakeCarOne. Therefore, this should be false if
        // we check if this is the selected car.
        XCTAssertFalse(mapViewModel.carMatchSelected(FakeData.fakeCarTwo))
    }
    
    func testSelectedCarKey() {
        XCTAssertTrue(mapViewModel.selectedKey == FakeData.fakeCarOne?.key)
    }
    
    func testCarAnnotations() {
        let carAnnotations = mapViewModel.carAnnotations(withObjects: FakeData.carArray)
        XCTAssertTrue(carAnnotations.count == 4)
    }
    
    func testSelectedCarRegion() {
        let fakeCar = FakeData.fakeCarOne
        let region = MKCoordinateRegion(center: fakeCar!.location,
                                        latitudinalMeters: MapData.regionMeters,
                                        longitudinalMeters: MapData.regionMeters)
        
        XCTAssertEqual(region, mapViewModel.selectedCarRegion())
    }

    func wipeDatabase() throws {
        // Clear down the DB
        let realm = try Realm()
        try realm.write {
            realm.deleteAll()
        }
    }
}
